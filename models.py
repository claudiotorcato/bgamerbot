from pony.orm import *
import os
import urllib.parse

db =  Database()

class Eleicao(db.Entity):
	nome = Required(str)
	edicao = Required(str)
	jogos = Set('Jogo')
	votos = Set('Voto')

class Jogo(db.Entity):
	nome = Required(str)
	eleicao = Required(Eleicao)
	votos = Set('Voto')

	def __repr__(self):
		return "%s" % (self.nome,)

class Usuario(db.Entity):
	user_id = Required(int)
	nome = Optional(str)
	telefone = Optional(str)
#	pode_votar = Optional(bool,default=False)
	votos = Set('Voto')
	sugestoes = Set('Sugestao')

class Voto(db.Entity):
	eleicao = Required(Eleicao)
	eleitor = Required(Usuario)
	jogo = Required(Jogo)
	posicao = Required(int)

class Sugestao(db.Entity):
	descricao = Required(str)
	usuario = Required(Usuario)
	grupo = Required(int)

urllib.parse.uses_netloc.append("postgres")
url = urllib.parse.urlparse(os.environ["DATABASE_URL"])

db.bind(
	'postgres',
	user=url.username,
	password=url.password,
	host=url.hostname,
	database=url.path[1:])
print(url)
db.generate_mapping(create_tables=True)
