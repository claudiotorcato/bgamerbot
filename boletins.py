# -*- coding: utf-8 -*-
BOLETINS = {
	59: """
Boletim Semanal de Notícias N° 59 (14.11.17 - 19.11.17)

Board Game
---------------------
 * O card game Bloodborn já está disponível para pronta-entrega na loja online da Galápagos Jogos.
 * Anunciados os lançamentos de fim de ano da Redbox Editora: Schotten Totten e The Big Book of Madness.
 * MS Jogos anuncia a primeira expansão para Chaparral: A Expansão pelo Oeste.

RPG
---------------------
 * Desafio dos Bandeirantes completa 25 anos desde sua primeira edição.
 * Lankhmar foi financiado em 24 horas no Catarse.

Editor: Cláudio Torcato
	""",
	58: """
Boletim Semanal de Notícias N° 58 (03.11.17 - 13.11.17)

Board Game
---------------------
 * Sábado, 2 de novembro, ocorrerá no Teresina Shopping o IV Cajuína Meeple.
 * Krosmaster Eternal Deck já está disponível na loja online da Galápagos Jogos.
 * Redbox Editora inicia a pré-venda do Bloodborne Card Game.
 * Começou o financiamento coletivo do Overdrive: A Batalha do Rock.

RPG
---------------------
 * Dungeonist disponibilizou novas fotos do marketplace que estão desenvolvendo.
 * Nebula: Piratas de Marduk alcançou a meta básica e duas metas estendidas.
 * Pensamento Coletivo anuncia o financiamento coletivo do Dragon King.
 * New Order Editora divulgou que lançará a 7ª edição de Call of Cthulhu.
 * Lampião Game Studio lançou Duello RPG durante o evento RPG no Grande Rio.
 * Começou o financiamento coletivo do cenário Lankhmar para Savage Worlds.

Editor: Cláudio Torcato
	""",
	57: """
Boletim Semanal de Notícias N° 57 (23.10.17 - 02.11.17)

Board Game
---------------------
 * O evento Cajuína Meeple agora tem uma página no Facebook.

RPG
---------------------
 * A primeira rodada de testes de Tagmar 3.0 foi concluída.
 * Iniciou o financiamento coletivo do Belregard, jogo da Lampião Game Studio.
 * Saiu a segunda edição da revista eletrônica NOW da New Order Editora.

Editor: Cláudio Torcato
	""",
	56: """
Boletim Semanal de Notícias N° 56 (10.10.17 - 22.10.17)

Board Game
---------------------
 * Teve início o financiamento coletivo do Meeple Heist, a ser publicado pela Papaya Editora.
 * Começou o financiamento coletivo do jogo para dois jogadores, Café Express, pela Potato Cat.
 * A New Order Editora lançou a pré-venda da expansão para o Shadowrun Crossfire, High Caliber Ops.
 * Meeple BR Jogos iniciou a pré-venda de Sociedade dos Salafrários e Ninja Camp.

RPG
---------------------
 * Alisson Vitório publicou a aventura para Dungeon World, Mistério em Porto Trys.
 * Redbox Editora iniciou a pré-venda da aventura O Culto do Caos Elemental.
 * Jambô Editora lançou o Manual dos Monstros: Criaturas Fantásticas para 3D&T Alpha.
 * Coisinha Verde inicia o financiamento coletivo do Nebula: Piratas de Marduk.
 * Pensamento Coletivo divulga que em breve haverão lançamentos de suplementos para Mutant: Ano Zero.
 * Foi lançado o O Jaguareté: O Encontro, com inspiração da cultura indígena. Ele será distribuído em escolas públicas.

Card Game
---------------------
 * Copag atualiza a lista de cartas banidas de Battle Scenes.
 * Divulgado pela Copag o lançamento de uma nova expansão para Battle Scenes: Confronto Aracnídeo.
 * Copag divulga a lista de lojas que participarão do lançamento da nova expansão para Pokémon, Sol e Lua - Invasão Carmim.

Editor: Cláudio Torcato	
	""",
	55: """
Boletim Semanal de Notícias N° 55 (03.10.17 - 09.10.17)

Board Game
---------------------
 * Começou o financiamento coletivo do Die, die, DIE!
 * Die, die, DIE financiado!
 
RPG
---------------------
 * O grupo Dungeonist está desenvolvendo um marketplace e convida autores e editoras a participarem da fase de testes.
 * Iniciado o financiamento coletivo do Sombras Urbanas.
 * Dias 18 e 19 acontece no Shopping Benfica em Fortaleza a 7ª edição do Soul+RPG.

Card Game
---------------------
 * William Jensen vence o Campeonato Mundial de Magic 2017.

Editor: Cláudio Torcato	
	""",
	54: """
Boletim Semanal de Notícias N° 54 (26.09.17 - 02.10.17)

Board Game
---------------------
 * Flick Game Studio está realizando um trade-in do jogo Fields of Green.
 * Devir Brasil divulga as novas expansões para Carcassone: Estalagens & Catedrais, A Torre 2ª Edição e a Princesa e o Dragão.
 * Conclave Editora divulgou o resultado do torneio de Room 25.
 * Meeple BR Jogos informa que todos os apoiadores de Karma Games receberão uma cópia de Clan of Caledonia. A pré-venda deve sair em breve.
 * Saíram as indicações ao Prêmio Cajuína Meeple edição 2 de 2017.

 
RPG
---------------------
 * Aster Editora e Lampião Game Studio divulgam a data do início do financiamento coletivo do Sombras Urbanas.
 * Saiu a terceira edição do Punho do Guerreiro, revista bimestral de Street Fighter RPG.
 * Raphael Lima publicou o jogo Sobreviventes no DriveThruRPG na modalidade Pague O Quanto Quiser.
 * Fabrica Editora informa que dia 13 de Outubro começa o financiamento coletivo do Desmortos (Undying) pelo Catarse.
 * Aster Editora divulga entre seus projetos para 2018, a publicação de The Ninja Crusade 2ª Edição no país.

Card Game
---------------------
 * A Copag divulga a data de lançamento da próxima expansão para Pokémon TCG, Lendas Luminescentes.

Editor: Cláudio Torcato	
	""",
	53: """
Boletim Semanal de Notícias N° 53 (18.09.17 - 25.09.17)

Board Game
---------------------
 * Devir Brasil divulgou o calendário de eventos do Pandemic Survival.
 * Devir Brasil lançou a expansão Comerciantes e Construtores para Carcassone.
 * Ocorreu um etapa do classificatório para o Nacional de Pandemic Survival em Brasília-DF.
 * Fantasy Flight Games divulga uma versão do Catan para o universo de Game of Thrones.

 
RPG
---------------------
 * New Order Editora anunciou no World RPG Fest  a 5ª edição do Ars Magica a ser lançada em 2018.
 * Rafão Araújo e Fábio Silva divulgam um novo jogo, Brumaria, de sua autoria.
 * O pessoal do Projeto Tagmar informou que os testes para a terceira edição de Tagmar começaram.
 * Bárbaros de Lemúria será lançado no Brasil em 2018 pela Aster Editoria (do jogo Travessias).
 * Ars Magica 5ª edição será publicado no Brasil pela New Order Editora em 2018.
 * Foi aberta a pré-venda do Kit do Mestre Old Dragon.

Card Game
---------------------
 * Galápagos publica um texto dando mais informações sobre a vinda do Legend of the 5 Rings.
 * Lucas de Almeida Matos vence o Battle Royal, torneio nacional de Marvel Battle Scenes.

Editor: Cláudio Torcato	
	""",
	52: """
Boletim Semanal de Notícias N° 52 (11.09.17 - 17.09.17)

Board Game
---------------------
 * Conclave Editora apresenta um dos próximos lançamentos, Gnomopolis, no evento UAI Board Games.
 * Conclave Editora anuncia o primeiro torneio por equipes de Room 25.
 * Kronos Games informa que trarão para o Brasil a versão deluxe e retail do The Flow History.
 * Ludofy trará outro jogo de Uwe Rosenberg: Indian Summer.

 
RPG
---------------------
 * Publicado suplemento para Mega City chamado Mega Tóquio - P.E.T - Polícia Especial de Tanques.
 * Saiu nova versão do Odú RPG cujo sistema usa búzios ao invés de dados.
 * O loja online Nerdz está com desconto de 30% em todos os livros de RPG.
 * Nosolorol anuncia RPGs oficiais de Rick & Morty e Steven Universe.
 * Saiu a edição nº 8 de revista Conexão Fate ano 1, cujo tema é Fantasia Medieval.
 * Foi publicada a Beholder Cego #14.
 * Late pledge do financiamento coletivo de Shadowrun 5ª edição já começou.

Editor: Cláudio Torcato
	""",
	51: """
Boletim Semanal de Notícias N° 51 (28.08.17 - 10.09.17)

Board Game
---------------------
 * Foi divulgada a nova capa do Die Die DIE e que o financiamento começará em outubro.
 * Iniciada a pré-venda de Dominion nas lojas parceiras da Conclave.
 * Lucas Dourado vence o IV Campeonato Loja Arcádia - Dogfight Raiz.

 
RPG
---------------------
 * Há 13 anos começava o Projeto Tagmar que viria a relançar Tagmar, o primeiro RPG brasileiro.
 * Redbox Editora informou que a pré-venda do Kit do Mestre de Old Dragon sai ainda em setembro.
 * Lampião Game Studio divulga novo jogo da casa, Duello RPG.
 * Coisinha Verde divulga um novo jogo, Nebula: Piratas de Marduk. O financiamento começa dia 11 de outubro.
 * Foi divulgada a versão alfa da quinta edição de Vampiro: A Máscara.

 Card Game
---------------------
 * Copag anunciou as datas e locais do League Cups de Pokémon TCG.

Editor: Cláudio Torcato	
	""",
	50: """
Boletim Semanal de Notícias N° 50 (21.08.17 - 27.08.17)
 
RPG
---------------------
 * A New Order Editora lança a revista New Order Warriors (NOW). A primeira e as duas edições seguintes serão gratuitas.
 * Saiu o resultado do Indie RPG Awards 2016. Na categoria de Indie Game of the Year venceu o Blade in the Dark de Jonh Harper publicado pela Evil Hat.

 Card Game
---------------------
 * Começou a Temporada 2017/2018 de Pokémon TCG da Copag. Haverá um torneio exclusivo onde os jogadores receberão cartas promo.

Editor: Cláudio Torcato	
	""",
	49: """
Boletim Semanal de Notícias N° 49 (14.08.17 - 20.08.17)

Board Game
---------------------
 * No Diversão Offline (DOFF), a Conclave anunciou Near and Far, Captain Sonar, Clank!, Clank! in the Space, Ethos e Inis como os próximos lançamentos.
 * No DOFF, a Mandala Jogos anunciou os seguintes lançamentos: Not Alone, Dr. Eureka e 
Photosynthesis.
 * No DOFF, a Galápagos Jogos anunciou os futuros lançamentos: Star Wars: Rebellion, Star Wars: Legion, Twilight Imperium 4ª Edição, Rising Sun, The Godfather, Arcadia Quest: Inferno, Pets e uma expansão de Dead of Winter que une as duas colônias.
 * No DOFF, a Grow confirma San Juan para outubro e Broom Service para dezembro.
 * No DOFF, a Meeple BR Jogos anuncia os seguintes lançamentos: Champions of Midgard, Lorenzo, Il Magnifico e Clans of Caledonia.
 * Meeple BR Jogos em parceria com a Arcano Games estão trabalhando no jogo Triora: Cidade das Bruxas.
 * No DOFF, a Retropunk Publicações anunciou um futuro lançamento: In the Name of Odin
 
RPG
---------------------
 * Na Gen Con 2017, a Monte Cook Games anuncia para o mês de setembro o seu próximo financiamento coletivo: Numenera 2: Discovery & Destiny.
 * Na Gen Con 2017, foram anunciados os vencedores no ENnie Awards 2017. O grande vencedor foi Tales from the Loop da editora sueca Free League Publishing.
 * No DOFF, a Retropunk divulga o Mouse Guard RPG como lançamento da editora para 2018.

Card Game
---------------------
 * No DOFF, a Galápagos Jogos anunciou um esquema de assinatura para o Legends of the Five Rings LCG.

Editor: Cláudio Torcato	
	""",
	48: """
Boletim Semanal de Notícias N° 48 (07.08.17 - 13.08.17)

Board Game
---------------------
 * Dia 3 de setembro ocorrerá o III Campeonato de X-Wing na Arcádia.
 * Devir lançou Cacao, um jogo de colocação de tiles.
 * Terminou o financiamento do Ancient Terrible Things onde foi arrecadado quase 130 mil Reais.
 
RPG
---------------------
 * Dia 27 de agosto ocorrerá mais um Encontro de RPG no Edifício The Doors.
 * Aster Editora anuncia um novo jogo a ser lançado brevemente, Outromundo.
 * Nova reimpressão de Dungeon World disponível na loja online da Secular Games.

Card Game
---------------------
 * João Lelis vence o Grand Prix São Paulo de Magic: The Gathering.

Editor: Cláudio Torcato	
	""",
	47: """
Boletim Semanal de Notícias N° 47 (31.07.17 - 06.08.17)

Board Game
---------------------
 * O GM Rafael Leitão venceu de forma invicta o V Aberto do Brasil de Xadrez ocorrido em Teresina.
 * Mandala Jogos anunciou que lançará Pot de Vin (nova versão de Sapotagem) em Essen.
 * Kronos Games informa que lançará no Brasil uma versão do Gold West diferente do original pois conterá material extra.
 * Crisis é a nova expansão para Star Realms que a Devir está lançando.
 * Devir divulga a chegada de Yangtze de Christopher Chung.

RPG
---------------------
 * Pré-venda de Thordezilhas: Sabres & Caravelas iniciou na loja online da Redbox.
 * O financiamento coletivo do Shadowrun 5ª Edição no Catarse bateu a meta básica.
 * Secular Games anunciou que fechou o contrato para a publicação da versão nacional da 2ª edição de Apocalypse World.
 * Diogo Nogueira lançou o PDF de Espadas Afiadas & Feitiços Sinistros na modalidade “pague quanto quiser” na loja online da Pensamento Coletivo.
 * Jambô Editora anuncia para o dia 31 de agosto o lançamento de A Jóia da Alma, novo romance no mundo de Arton, escrito por Karen Soarele.
 * Espadas Afiadas & Feitiços Sinistros está em pré-venda na loja online da Pensamento Coletivo.

Card Game
---------------------
 * O Grand Prix São Paulo de Magic: The Gathering ocorrerá entre os dias 11 a 13 de agosto.
 * Devir lança Epic da White Wizard Games.

Editor: Cláudio Torcato	
	""",
	46: """
Boletim Semanal de Notícias N° 46 (24.07.17 - 30.07.17)

Board Game
---------------------
 * Saiu a resultado de uma pesquisa da Redbox Editora para o nome do novo jogo de Rodrigo Rego e Rafa Miqueleto. O nome será Paper Town.
 * San Juan é um dos próximos lançamentos da Grow.
 * Entre os dias 4 e 6 de agosto teremos em Teresina o V Aberto do Brasil de Xadrez 2017. Três Grandes Mestres estão inscritos. 

Card Game
---------------------
 * Paulo Vitor Damo da Rosa venceu o Pro Tour Kyoto, importante torneio de Magic: The Gathering. Ao vencer, conquistou também o título de Player of the Year 2017.

Editor: Cláudio Torcato	
	""",
	45: """
Boletim Semanal de Notícias N° 45 (17.07.17 - 23.07.17)

Board Game
---------------------
 * Aconteceu o III Cajuína Meeple na praça de alimentação do Rio Poty Shopping.
 * Mandala Jogos divulgou novos jogos para pronta-entrega: Taverna e toda a coleção de Dwar7s.
 * Devir Brasil publicou o resultado do Campeonato Brasileiro de Carcassone. O campeão foi Guilherme Ziegler da Silva, classificando-se para o mundial em Essen.
 * Flick Game Studio iniciou a pré-venda de Alhambra e Robotroc.
 * Kingdomino venceu o Spiel des Jahres 2017 e EXIT: The Game venceu o Kennerspiel des Jahres 2017.

RPG
---------------------
 * Retropunk Publicações abriu a pré-venda do Hora de Aventura RPG.
 * Jambô Editora iniciou a pré-venda de A Patrulha da Noite, suplemento para Guerra dos Tronos RPG.
 * Aster Editora lançou a pré-venda do Travessias.

Editor: Cláudio Torcato	
	""",
	44: """
Boletim Semanal de Notícias N° 44 (10.07.17 - 16.07.17)

Board Game
---------------------
 * Game of Boards anuncia a pré-venda de Bohnanza.
 * Devir Brasil completa 30 anos de fundação.
 * Kronos Games divulga atualizações sobre o projeto da edição limitada de Belfort.
 * Kronos Games informa que começou a campanha do Crusaders da TMG e formas de adquirir o jogo por meio deles. Horas depois o jogo já estaria financiado.
 * Lançamento do Anime Saga ocorreu na Game of Boards, dia 15 de julho.
 * Iniciou uma campanha no Catarse de um dungeon build grid isométrico e quadriculado.

RPG
---------------------
 * Solar Entretenimento anuncia que iniciarão os envios dos produtos da pré-venda e os últimos itens do financiamento coletivo do FATE para a semana de publicação deste boletim.
 * Solar Entretenimento anuncia que se tornará um selo de criação, um estúdio-editora. A produção e distribuição de parte dos produtos ficará por conta da Meeple BR Jogos.
 * Meeple BR Jogos anuncia que fará a distribuição dos produtos da Solar Entretenimento por meio de uma parceria com a própria. 
 * Retropunk Publicações divulga a data de lançamento da pré-venda do Hora de Aventura Roleplaying Game. Dia 21 de julho.
 * Iniciou a pré-venda de Pátria, primeiro livro da linha de literatura de D&D da Jambô Editora.

Card Game
--------------------
 * Copag divulga a data do lançamento do box evolução Turbo Arcanine que será 4 de agosto.

Editor: Cláudio Torcato e Francisco Hellison	
	""",
	43: """
Boletim Semanal de Notícias N° 43 (03.07.17 - 09.07.17)

Board Game
---------------------
 * Deep6 é mais um jogo anunciado pela Redbox Editora a ser lançado em 2018. 
 * Cangaço, jogo de Sanderson Virgulino, será publicado pela Redbox Editora.
 * Ancient Terrible Things entrou em financiamento coletivo no Catarse e já bateu a meta em menos de 10 horas. 
 * Akigam, um card game da Papaya Editora, está em financiamento coletivo no Catarse. Até o momento estão com 64% da meta para financiá-lo.
 * Alhambra, jogo vencedor do Spiel de Jahres, está chegando ao Brasil pelas mãos da Flick Game Studio.

RPG
---------------------
 * Saíram as indicações ao ENnie Award 2017.
 * Pátria, primeiro volume da trilogia do elfo negro, estará em pré-venda este mês pela Jambô Editora. O livro conta a origem de Drizzt Do’Urden, famoso elfo negro do não menos famoso Forgotten Realms, cenário de D&D.
 * Causos RPG é o nome do próximo lançamento do Lampião Game Studio, inspirado no universo ficcional dos quadrinhos de Eberton Ferreira, sobre releitura de lendas do folclore nacional.

Card Game
--------------------
 * Anunciado o local do segundo Regional de Battle Scenes e será no Rio Grande do Sul.

Editor: Cláudio Torcato e Francisco Hellison	
	""",
	42: """
Boletim Semanal de Notícias N° 42 (26.06.17 - 02.07.17)

Board Game
---------------------
 * Empires: Age of Discovery foi o vencedor do Prêmio Cajuína Meeple Edição 2017.1.
 * III Cajuína Meeple, encontro de aficionados por jogos de tabuleiro, acontecerá no dia 22 de julho no terraço da praça de alimentação do Shopping Rio Poty (em Teresina).
 * Colosseum Emperors Edition em estoque na loja online da Game of Boards.
 * A Redbox Editora lançará na próxima semana o financiamento coletivo no Catarse do Ancient Terrible Things.
 * Em novembro a Grow lançará uma nova versão do clássico War, War Vikings.
 * A expansão Maldade Caótica do Covil: Mestre das Trevas está em pré-venda.

RPG
---------------------
 * A Redbox começou a soltar os previews do ThordeZilhas, um cenário de sabres & caravelas para Old Dragon.

Editor: Cláudio Torcato e Francisco Hellison	
	""",
	41: """
Boletim Semanal de Notícias N° 41 (19.06.17 - 25.06.17)

Board Game
---------------------
 * A Meeple BR Jogos inicou a pré-venda de Orléans e sua entrega será no mês de agosto.
 * Terminou o financiamento coletivo do Anime Saga com 161% alcançados.
 * O blog E aí, tem Jogo? postou um texto explicando o Above and Below, futuro lançamento da Conclave Editora.
 * A Mandala Jogos divulga imagens do lançamento para os próximos meses, O Albergue Sangrento.
 * Charterstone foi apresentado como futuro lançamento da Ludofy.
 * Masquerade é o futuro lançamento anunciado pela Editora Redbox.

RPG
---------------------
 * O universo de A Bandeira do Elefante e da Arara ganhará um RPG e será lançado pela Devir.
 * A Redbox Editora divulga que a Moostache Books, Dmand Game Studio e Flying Ape são os mais novos selos da editora.
 * Crônicas de Thedas, uma antologia de aventuras para Dragon Age RPG está em pré-venda na loja da Jambô Editora, a Nerdz.

Editor: Cláudio Torcato
	""",
	40: """
Boletim Semanal de Notícias N° 40 (12.06.17 - 18.06.17)

Board Game
---------------------
 * O jogo Gold West está em pré-venda nas lojas. Na loja online da Kronos Games, apenas começará no final do mês.

RPG
---------------------
 * O cenário Mítica vai ganhar uma atualização pelo Estúdio Mundos de Papel, convertendo suas regras de D20 para Savage Worlds.
 * Gatos Especiais está com novo financiamento coletivo no Catarse.

Card Game
---------------------
 * Fernanda Nunes venceu o Campeonato Brasileiro de V:TES 2017 que aconteceu em Manaus no dia 17 de junho.
 * Os eventos regionais de Marvel: Battle Scenes começam a serem definidos.

Editor: Cláudio Torcato
	""",
	39: """
Boletim Semanal de Notícias N° 39 (05.06.17 - 11.06.17)

Board Game
---------------------
 * Mandala Jogos anuncia a pré-venda do Covil: Mestres das Trevas, jogo do brasileiro Luis Brueh, que está em financiamento coletivo no Kickstarter pela Vesuvius Media.
 * A Conclave Editora já disponibilizou para download o livro de regras do Great Western Trail que está em pré-venda.
 * A Meeple BR Jogos emitiu uma nota sobre o manual do Fireteam Zero.

RPG
---------------------
 * Saiu a 3ª edição da revista Aventuras Infinitas. Nela uma aventura inspirada em Avatar: A Lenda de Aang.
* Goddess Save the Queen é um novo RPG brasileiro a ser publicado pela Redbox Editora. Os autores são Carolina Neves e Júlio Matos.
* Pesadelos Terríveis, jogo narrativo de horror de Jorge Valpaços, publicado pela Avec Editora, está em pré-venda na loja online e também na Amazon BR.

Editores: Cláudio Torcato, Francisco Hellison e Gabriel Bragança	
	""",
	38: """
Boletim Semanal de Notícias N° 38 (29.05.17 - 04.06.17)

Board Game
---------------------
 * Chegaram no mercado brasileiro as expansões da Wave 10 de Star Wars X-Wing: Miniatures Game.
 * A Paper Games informa que colocar em breve, por pré-venda, o jogo Suburbia e a expansão Suburbia Inc.
 * A Krono Games avisa aos apoiadores do Kickstarter que Colossus e Yokohama estão desembarcando no Brasil.
 * O jogo Great Western Trail está em pré-venda na loja online da Conclave Editora.

RPG
---------------------
 * Segunda-feira, dia 5 de junho, é o último dia para apoiar o financiamento coletivo do Dungeon Crawl Classics RPG na modalidade late pledged.

Editores: Cláudio Torcato, Francisco Hellison e Gabriel Bragança	
	""",
	37: """
Boletim Semanal de Notícias N° 37 (22.05.17 - 28.05.17)

Board Game
---------------------
 * A pré-venda de Swords and Bagpipes começa dia 2 de junho pela Mamute Jogos.
 * Mansions of Madness 2ª edição já está nas lojas parceiras da Galápagos Jogos.

RPG
---------------------
 * A New Order Editora informou que o impresso de Kuro já está no estoque e em breve enviarão para os compradores da pré-venda.
 * Dia 29 de maio começa a pré-venda do Classroom Deathmatch.
 * O próximo lançamento do Lampião Game Studio sairá pela Editora Avec e se chama Pesadelos Terríveis, jogo que expande o universo da HQ Beladona de Ana Recalde e Denis Mello.

Editores: Cláudio Torcato, Francisco Hellison e Gabriel Bragança

	""",
	36: """
Boletim Semanal de Notícias N° 36 (15.05.17 - 21.05.17)

Board Game
---------------------
 * PaperGames anuncia diversos lançamentos para as próximas semanas: Luna, Suburbia, Suburbia Inc, Kingdominio, entre outros.
 * Studio Teia de Jogos iniciou o financiamento coletivo no Catarse do card game Seguem Alterações do Cliente.
 * GetBit! entrou em pré-venda pela Conclave Editora.

RPG
---------------------
 * Pensamento Coletivo abriu pré-venda e pronta-entrega de diversos produtos: Fate Sistema Básico, Ferramentas do Sistemas (suplemento), versões impressas de livros antes disponíveis somente em PDF, conjunto de dados Fudge, entre outras.
 * Financiamento Coletivo no Catarse no Universo RPG, sistema e dois cenáriosa acompanhando.

Editores: Cláudio Torcato, Francisco Hellison e Gabriel Bragança	
	""",
	35: """
Boletim Semanal de Notícias N° 35 (08.05.17 - 14.05.17)

Board Game
---------------------
 * Lançamento do Card Game Scippio, na loja Arcádia.
 * Legião Jogos publicou o primeiro post do diário de desenvolvimento do Scippio na Ludopedia. Empresa piauiense iniciando de forma organizada o seu caminho.
 * Dia 21 de Maio ocorrerá o III Campeonato da Arcádia de Star Wars X-Wing: The Miniatures Game na Arcádia Geek Store em Teresina.
 * Chegou ao fim o financiamento coletivo do RPGQuest. O jogo alcançou todas as metas estendidas e até a edição desse boletim havia arrecadado mais de R$ 176.000,00.
 * Saíram as indicações para o Origins Awards, uma das principais premiações do mundo dos board games e RPGs.

RPG
---------------------
 * O Quickstart para Shadowrun 5ª Edição foi disponibilizada pela New Order Editora nos preparativos da pré-venda do jogo.
 * Começou o financiamento coletivo do Ahoy!! de Don Nobru.
 * A Jambô Editora abriu a pré-venda do Arquivos do Sabre, suplemento para 3D&T e Brigada Ligeira Estelar.
 * Chegou ao fim o financiamento coletivo do cenário para Savage Worlds, Weird Wars II, alcançando a meta básica nos últimos dias.
 * Kenneth Hite é o lead designer no Vampire: The Masquerada 5th Edition.

Card Game
---------------------
 * O italiano Danilo Torrisi é o novo campeão europeu de VTES, ao vencer o VTES European Championship (Day 2).

Editores: Cláudio Torcato, Francisco Hellison e Gabriel Bragança	
	""",
	34: """
Boletim Semanal de Notícias N° 34 (01.05.17 - 07.05.17)

Board Game
---------------------
 * A Redbox Editora atualizou sua página de status dos projetos futuros.
 * A Redbox Editora anunciou o lançamento do Frogriders e sua parceira com as empresas Eggertspiele e a Pegasus Spiele.
 * A Retropunk Publicações iniciou a pré-venda do Fat Food.
 * A Conclave Editora anuncia que trará para o Brasil o Clank!.
 * RPGQuest chega nos últimos dias de financiamento com várias metas estendidas alcançadas.
 * Os campeonatos mundiais 2017 da Fantasy Flight Games de jogos com o tema Star Wars (Star Wars: The Card Game, X-Wing Miniatures Game, Armada, Imperial Assault e Destiny) terminaram no dia 7 de maio.
 * Últimos dias do Kickstarter do Brass de Martin Wallace.
 * Últimos dias do Catarse do Weird Wars II.

RPG
---------------------
 * A Editora Dimensão Nerd iniciou a pré-venda do In Nomine.
 * Dia 8 de maio começa a pré-venda do Don’t Rest Your Head pela Fábrica Editora.
 * A campanha de financiamento do Evolution Pulse terminou sem que a meta básica fosse alcançada. A Fábrica Editora agradeceu o apoio de todos.
 * Começou o late pledge do Dungeon Crawl Classics RPG.

Editores: Cláudio Torcato, Francisco Hellison e Gabriel Bragança	
	""",
	33: """
Boletim Semanal de Notícias N° 33 (24.04.17 - 30.04.17)

Board Game
---------------------
 * Redbox Editora e Ace Studios se juntam para a produção do Die Die, DIE!, futuro lançamento da Ace Studios.
 * Pré-venda do Terraforming Mars começou.

RPG
---------------------
 * Teve inicio o KickStarter da segunda edição de Eclipse Phase. Um RPG de sobrevivência transumanista; onde a morte foi superada através de tecnologias biologicas avançadas. https://www.kickstarter.com/projects/507486226/eclipse-phase-second-edition.
 * O financiamento coletivo do cenário Evolution Pulse (com regras para FATE/FAE) chega aos últimos dias.
 * Pensamento Coletivo anunciou a pré-venda de seus livros impressos (provalmente Fate Básico e Fate Ferramentas do Sistema) para o dia 1º mas por problemas com na loja virtual, adiou para o dia seguinte.
 * Classroom Deathmatch entrará em pré-venda da Redbox Editora.
 * A Arsenal Editora começou os preparativos de envio dos impressos do Our Last Best Hope para os apoiadores do financiamento e já é possível comprá-los via PagSeguro.
 * New Order Editora dá sinais de que abrirá o late pladge do 7º Mar em maio.

Card Game
---------------------
 * Dia 28 aconteceu o lançamento oficial da nova coleção de Magic: The Gathering, Amonkhet, inspirada no Antigo Egito.
 * Copag anuncia o lançamento da nova coleção de Pokémon, Sol e Lua: Guardiões Ascendentes. 

Editores: Cláudio Torcato, Francisco Hellison e Gabriel Bragança

	""",
	32: """
Boletim Semanal de Notícias N° 32 (17.04.17 - 23.04.17)

Board Game
---------------------
 * MS Jogos anuncia data para as vendas de O.V.I.N.I 2e. Vendas começam dia 08/05 com apenas 250 cópias disponíveis. 
 * MS Jogos também divulga lançamento de seu 9º jogo. Xingu tem data prevista para o 2º semestre de 2017.
 * Devir inicia pré venda de Alquimistas: O Golem do Rei (expansão), O Vale dos Mercadores e também pré venda de volta de estoque de Ubongo e Castellers. Todos eles com previsão de entrega para junho/17
 * Ludofy anuncia lançamento de Bärenpark. Jogo de construção de parque de ursos ainda não teve informações de vendas divulgada.
 * Ace Studios inicia pré venda de seu novo card game Medievalia. Jogo tem previsão de entrega para maio/17.
 * Pensamento Coletivo inicia envios de Imperial 2030. Após alguns meses de atraso, por enquanto, somente os apoiadores do financiamento coletivo estão recebendo seus jogos.

RPG
---------------------
 * RedBox anuncia parceria em Kickstarter para trazer The Shotgun Diaries de John Wick. Nenhuma outra informação foi divulgada.
 * Editora Fábrica anuncia que abrirá pré-venda do jogo Don’t Rest Your Head em maio.

Card Game
---------------------
 * Fantasy Flight Games divulga quando lançarão o novo Legend of the Five Rings para o quarto trimestre de 2017.

Editores: Cláudio Torcato, Francisco Hellison e Gabriel Bragança	
	""",
	31: """
Boletim Semanal de Notícias N° 31 (10.04.17 - 16.04.17)

Board Game
---------------------
 * Conclave inicia pré venda de Barony, Barony Sorcery (expansão), Ultimate Warriorz e Jórvík. 
 * Galápagos Jogos inicia pré venda de Mr. Jack e King of Tokyo 2.0 e renovação de estoque de Cyclades. 
 * Mandala Jogos libera pré venda em conjunto com o Kickstarter de Dead Man's Doubloons. Jogo tem duas versões: Básica e de Luxo.
 * PaperGames inicia envios de pré venda de Port Royal. Jogo já se encontra no estoque de algumas lojas. 
 * Ludofy divulga data de início de produção de Caverna. Produção deve ter início no fim de abril/17 com previsão de chegada nas lojas em julho/agosto/17.
 * Labyrinx foi financiado na primeira semana de campanha no Catarse.

RPG
---------------------
 * Jambô Editora anunciou que irá publicar a linha de literatura de Dungeons & Dragons em português. A primeira série a ser publicada será A Lenda de Drizzt, ambientada no cenário de Forgotten Realms.
 * Saiu a Conexão Fate Nº 5 cujo tema abordado é Espionagem.
 * O financiamento do Espadas Afiadas & Feitiços Sinistros está chegando ao fim.

Editores: Cláudio Torcato e Gabriel Bragança
	""",
	30: """
Boletim Semanal de Notícias N° 30 (03.04.17 - 09.04.17)

Board Game
---------------------
 * Meeple BR anuncia o lançamento de Orléans com previsão para o segundo semestre de 2017. 
 * Cool Mini or Not abre inscrições para testes de seu novo jogo de miniaturas que será baseado na série de livros “As crônicas de gelo e fogo” de George R. R. Martin. Como já aconteceu com outros jogos da empresa, testes também serão feitos no Brasil.
 * RedBox inicia o financiamento coletivo de Labyrinx. Jogo alcançou 20% de sua meta inicial em 1 hora.
 * RedBox inicia pré venda de Fief Francia 1429. Estimativa de entrega para junho/17.
 * Grow libera linha de produtos focados em criação de jogos em sua loja virtual. 
 * Termina o financiamento de Rising Sun. Novo jogo de Eric Lang com a Cool Mini or Not arrecadou mais de 4 milhões de dólares com o apoio de mais de 30 mil pessoas.
 * Cool Mini or Not já anuncia 2 temporada de Zombicide Black Plague. Batizado de Zombicide Green Horde, jogo tem seu financiamento previsto para fim de maio/17. 
 * Em comunicado, Renegade Games anuncia parceria com a Conclave Editora e Devir que iram trazer Clank! A Deck building Adventure, Fuze (ambos pela Conclave) e Lanterns, The harvest festival (Devir).
 * Ludofy inicia envios de Viticulture e disponibiliza manual oficial em português na Ludopedia. 
 * Mandala Jogos renova o estoque de Terra Mystica em seu site.
 * Mandala Jogos anuncia uma parceria com a ThunderGryph Games para trazer um jogo que está sendo financiado no Kickstarter, o Dead Man’s Doubloons.

RPG
---------------------
 * Mandala Jogos anuncia produção de seu primeiro livro-jogo. Captive será totalmente ilustrado em formato de história em quadrinhos.
 * Retropunk vai trazer o Hora de Aventuras RPG.
 * Fábio Silva (pela Pluma Publicações) informa que iniciará o financiamento coletivo do jogo Gatos Espaciais cuja arrecadação em sua maior parte irá para o tratamento de duas gatinhas e mais umas ONGs brasileiras. Começa dia 18 de Abril. O sistema tem inspiração no FATE Acelerado.
 * Redbox Editora anunciou a reimpressão da edição revisada de O Forte das Terras Marginais para Old Dragon.

Editores: Cláudio Torcato e Gabriel Bragança	
	""",
	29: """
Boletim Semanal de Notícias N° 29 (27.03.17 - 02.04.17)

Board Game
---------------------
 * Anderson “Panda” Pinheiro sagrou-se campeão nacional de Star Wars: X-Wing. 
 * Gigante Jogos inicia pré venda de Trajan. Previsão de entrega já para o mês de abril.
 * RedBox divulga data de início do seu primeiro projeto em financiamento coletivo. Financiamento do jogo Labyrinx terá início dia 03 de abril.
 * Lançamento pronta-entrega de OVNI tem data para acontecer: 8 de Maio.

RPG
---------------------
 * A Arsenal Editora informou que os exemplares do story game Out Last Best Hope que foi financiado em 2014 finalmente começarem a ser enviados para os apoiadores.
 * Saiu um edição especial da revista Aventuras Infinitas sobre Evolution Pulse, cenário para FATE/FAE que encontra-se em financiamento coletivo, contendo uma aventura para ser usada com o fastplay.
 * Última semana do financiamento do Travessias que já alcançou sua meta básica há algumas semanas.
 * A Editora Dimensão Nerd avisa que em final de maio ou começo de junho lançará a versão nacional de In Nomine.

Editor: Cláudio Torcato e Gabriel Bragança
	""",
	28: """
Boletim Semanal de Notícias N° 28 (20.03.17 - 26.03.17)

Board Game
---------------------
 * Tivemos o início da ABRIN (Feira Nacional de Brinquedos). Segundo alguns visitantes, o lançamento de Imperial Assault foi realmente cancelado e podemos esperar por novas expansões de Eldritch Horror. A Grow confirma o lançamento de San Juan e Broom Service, fora o lançamento de The Castles of Burgundy com lançamento para julho.
 * Galápagos atualiza sua página “Em Breve”. Lá podemos ver os lançamentos de Mr. Jack, Dixit - Revelations e várias novas naves de X-Wing. Também teremos renovação de estoque de Cyclades e King of Tokyo (já com a nova arte). 
 * Iniciado financiamento coletivo do card game Anime Saga.
 * Gigante Jogos anunciou em sua página no Facebook que pré venda de Trajan começará em breve.
 * RPGQuest alcançou a meta básica do seu financiamento coletivo.

 RPG
---------------------
 * D&D 5ª Edição chegará ao Brasil pela Fire on Board mas sob polêmicas. Após alguns dias de polêmicas, Gale Force 9 suspende lançamento da edição de D&D em português.
 * Começou o late pledge do 7º Mar.
 * Terminou o financiamento coletivo do Dungeon Crawl Classics RPG. Quase chegam a marca dos R$ 100.000,00 (mas faltavam pagamentos a serem confirmados no momento da edição do boletim).
 * A Fábrica Editora liberou um fastplay do Evolution Pulse que está em financiamento coletivo.
 * Novas aventuras para Savage Worlds e o cenário de Deadlands na Retropunk.

Editor: Cláudio Torcato e Gabriel Bragança
	""",
	27: """,
Boletim Semanal de Notícias N° 27 (13.03.17 - 19.03.17)

Board Game
---------------------
 * RedBox anuncia novos lançamentos de Kanagawa e Welcome to the Dungeon e também anuncia novos tamanhos para sua linha de sleeves .
 * Meeple BR inicia pré venda de Jim´s Henson: Labyrinth, jogo com tema do filme Labirinto com David Bowie. 
 * Devir anuncia pré venda de Codenames Pictures e Cartagena e volta de estoque de Agricola, Star Realms e Homes.
 * MS Jogos anuncia OVNI 2a edição. Lançamento com pronta entrega para maio/17.
 * Fire on Board inicia pré venda de novo party game Assuntos Internos. Envios previstos para março/17.
 * Vitor Fernando Silva vence o Grand Prix Porto Alegre de Magic: The Gathering e consegue vaga para o Pro Tour Hour of Devastation em Kyoto, Japão.

 RPG
---------------------
 * Wizards of the Coast anuncia lançamento de aplicativo oficial para  coordenar seções e campanhas de Dungeons and Dragons 5e. Aplicativo terá entre outras funções, compêndio oficial e fichas editáveis para personagens.
 * Ferramentos do Sistema, aguardado suplemento para FATE, está a venda em formato digital no site da Solar Entretenimento.
 * Déloyal, RPG brasileiro sobre aventuras pulps de resistência e liberdade, está disponível para pronta entrega no site da Pensamento Coletivo.
 * Jambô Editora divulgou seu cronograma de lançamento para 2017, com novidades para as linhas da editora como 3D&T, Dragon Age, Mutantes & Malfeitores, Guerra dos Tronos, Reinos de Ferro e Tormenta.
 * Dia 21 termina o financiamento coletivo do Dungeon Crawl Classics RPG com várias metas extras batidas até a publicação deste boletim.

Editor: Cláudio Torcato e Gabriel Bragança
	""",
	26: """
Boletim Semanal de Notícias N° 26 (06.03.17 - 12.03.17)

Board Game
---------------------
 * Galápagos jogos anuncia início de pré venda para Tail Feathers, 7 Wonders Duel e reposição de estoque de 7 Wonders. Previsão de entrega para segunda quinzena de março.
 * Ludofy inicia pré venda de Viticulture. Previsão de entrega para abril.
 * PaperGames inicia pré venda de Port Royal. Previsão de entrega ara abril.
 * Mandala Jogos anuncia venda do lançamento Pocket Imperium e volta de estoque de Coup. Venda já está liberada no site da empresa e lojas parceiras.
 * O grande vencedor do Golden Geek Awards 2016 foi Scythe, ganhando na categoria de Melhor Jogo e mais outras.
 * Iniciou o financiamento coletivo do RPGQuest, jogo de Marcelo Del Debbio.

 RPG
---------------------
 * Foi relançado o financiamento coletivo de um jogo escolar de RPG chamado Histórias do Brasil.
 * Fábrica Editora está com seu primeiro jogo em financiamento coletivo: Evolution Pulse.
 * Retropunk lança o cenário Weird Wars II para Savage Worlds no Catarse.

Editor: Cláudio Torcato e Gabriel Bragança	
	""",
	25: """
Boletim Semanal de Notícias N° 25 (27.02.17 - 05.03.17)

Board Game
---------------------
 * II Cajuína Meeple ocorrerá sábado, dia 11, na praça de alimentação do Shopping Rio Poty.
 * O jogo Imagine foi lançado pela Galápagos Jogos.
 * Terra Mystica e Space Cantina venceram o Prêmio Ludopedia 2016.


RPG
---------------------
 * Pré-venda do A Penny for my Thoughts na loja da Redbox Editora.
 * Retropunk lançou a aventura Deadlands: Um Conto de Dois Assassinos em PDF.

Editor: Cláudio Torcato
	""",
	24: """
Boletim Semanal de Notícias N° 24 (20.02.17 - 26.02.17)

Board Game
---------------------
 * Galápagos Jogos trará 7 Wonders Duel, A Game of Thrones: The Iron Throne e Mr. Jack.
 * Galápagos Jogo está estruturando um ciclo de torneios para Krosmaster Arena.
 * Meeple BR Jogos anuncia para breve Terraforming Mars.
 * Pré-venda do Mottainai na Ludofy com previsão de lançamento na segunda quinzena de março.
 * Redbox Editora anuncia os jogos da parceria com a Iello, Kanagawa e Welcome to the Dungeon.

RPG
---------------------
 * A comunidade Narrador Eficiente divulgou o bot DWAppBOT, desenvolvido por Bruno Rodrigues e Daniel Tavares, que ajuda a jogar campanhas de Dungeon World pelo Telegram.
 * GURPS é oficialmente abandonado no Brasil (porque na prática já era).
 * Redbox Editora anuncia seu primeiro Adventure Path para Old Dragon, Delírios de X’agyg.
 * Com o late pledge de Rastro de Cthulhu foi possível chegar a meta que libera a versão digital da aventura The Long Con.

Editor: Cláudio Torcato
	""",
	23: """
Boletim Semanal de Notícias N° 23 (13.02.17 - 19.02.17)

Board Game
---------------------
 * Redbox Editora apresenta a imagem do primeiro board game nacional da empresa, o Tsukiji, cujo financiamento coletivo será simultâneo no Brasil e na Europa.
 * La Muerte entrou em financiamento coletivo no Kickante.
 * Jórvik está disponível para venda entrega imediata no site da Conclave Editora.
 * Saiu o resultado da votação do Top 10 dos jogos solos da 1 Player Guild Brasil.

RPG
---------------------
 * Pré-venda do The Strange ocorre entre os dias 15 e 20 de Janeiro na loja online de New Order Editora.
 * A Ordem dos Últimos, suplemento para Dungeon World, desenvolvimento por Júlio Matos, está a venda no site da Coletivo Secular em formato PDF por R$ 16,90.
 * Igor Moreno, autor do Chopstick e Space Dragon, abriu um Apoia.se para ajudá-lo no desenvolvimento de novos jogos. 

Editores: Cláudio Torcato e Gabriel Bragança
	""",
	22: """
Boletim Semanal de Notícias N° 22 (06.02.17 - 12.02.17)

Board Game
---------------------
 * Expansão Chibis do Takenoko está disponível para venda na loja online da Galápagos e nos revendedores a partir de 12 de fevereiro.
 * Dia 11 ocorreu em Teresina o torneio 1ª Batalha do Berçário de Star Wars: X-Wing. O vencedor foi Lucas Dourado.
 * O jogo Dogs do Marcos Macri está em financiamento coletivo no Kickstarter para o lançamento de uma versão na língua inglesa pela editora Gray Mass Games.

RPG
---------------------
 * Começou o financiamento coletivo do Travessias para a produção de uma versão impressa. Ele é um jogo focado em desapego, amadurecimento e sobrevivência.
 * Última semana do late pledge de Rastro de Cthulhu. O objetivo é arrecadar o suficiente para lançamento da cenário de campanha The Long Con.

Editores: Cláudio Torcato e Gabriel Bragança
	""",
	21: """
Boletim Semanal de Notícias N° 21 (30.01.17 - 05.02.17)

Board Game
---------------------
 * Do dia 10 a 12 de Fevereiro começa o Ciclo de Torneios Star Wars: Destiny com eventos acontecendo em várias lojas pelo país.
 * Arcádia divulga a Primeira Temporada da Liga Teresinense de Summoner Wars.

RPG
---------------------
 * Sairá por meio de financiamento coletivo do jogo Espadas Afiadas & Feitiços Sinistros pela editora Pensamento Coletivo.
 * Infected! é o terceiro jogo no catálogo da Fabrica Editora a ser anunciado pela mesma.

Editores: Cláudio Torcato e Gabriel Bragança

	""",
	20: """
Boletim Semanal de Notícias N° 20 (23.01.17 - 29.01.17)

Board Game
---------------------
 * Retropunk Publicações lançará o jogo Fat Food em março.

RPG
---------------------
 * Saiu a esperada edição 115 da Dragão Brasil.
 * A Fábrica Editora prever para o primeiro trimestre o início do financiamento coletivo do jogo italiano Evolution Pulse que usa o sistema Fate Básico ou Fate Acelerado.
 * A Fábrica Editora informa que poderá fazer uma campanha de financiamento coletivo para a produção de uma versão física e digital do The Veil, jogo cyberpunk que usa a Apocalypse Engine. 

Editores: Cláudio Torcato e Gabriel Bragança
	""",
	19: """
Boletim Semanal de Notícias N° 19 (16.01.17 - 22.01.17)

Board Game
---------------------
 * PaperGames em breve iniciará a pré-venda do Oh My Goods!
 * Criado um blog para a comunidade teresinense do jogo Star Wars X-Wing: The Miniatures Game.
 * Galápagos Jogos anunciou para o dia 1º e 2 de Abril o Torneio Nacional de Star Wars X-Wing.
 * Mandala Jogos começou a vender a um preço promocional os jogos Pablo e Ascencion: O Retorno do Caído.

RPG
---------------------
 * Começou o financiamento coletivo do Dungeon Crawl Classics RPG no Catarse.
 * Financiamento do 7º Mar foi concluído com êxito estrondoso e até o momento da publicação deste boletim, não sabemos se superou Shadow of Demon Lord.
 * Finalizado com sucesso o financiamento do Guia de Classes para Old Dragon.
 * Redbox Editora anuncia para fevereiro a pré-venda de A Penny For My Thoughts.
 * Retropunk Publicações anuncia que em março iniciará o financiamento coletivo do cenário para Savage Worlds, Weird War 2. Outros suplementos virão como meta estendida.
 * Retropunk Publicações projeta para o meio do ano o financiamento do Castle Falkenstein.
 * Retropunk Publicações anuncia para o segundo semestre o financiamento do cenário Lankhmar: City of Thieves para Savage Worlds.
 * Entre os lançamentos previstos para 2017 da Retropunk, não estão mais previstos jogos da Pelgrane Press como Esoterrorists e Night’s Black Agents.

Editores: Cláudio Torcato e Gabriel Bragança
	""",
	18: """
Boletim Semanal de Notícias N° 18 (09.01.17 - 15.01.17)

Board Game
---------------------
 * A edição 2017 do Diversão Offline acontecerá nos dias 19 e 20 de agosto 
 * Húsz deve ser publicado ainda no final de janeiro pela Lemonpie Games

RPG
---------------------
 * O financiamento coletivo da nova impressão de Rastro de Cthulhu alcançou sua meta básica nas últimas horas. Os late pledges já estão disponíveis na loja online da editora.
 * A campanha de financiamento coletivo do suplemento para Old Dragon, Guia de Classes, atingiu sua meta básica e mais duas metas estendidas até o momento desta publicação.

Editores: Cláudio Torcato e Gabriel Bragança
	""",
	17: """
Boletim Semanal de Notícias N° 17 (02.01.17 - 08.01.17)

Board Game
---------------------
 * Encontra-se em financiamento coletivo o jogo para duas pessoas, Tao Long, O Caminho do Dragão. É um jogo da Octo Ludistudio que está em pré-venda no Brasil, mas faz parte de um financiamento internacional no Kickstarter.
 * A Histeria Games divulgou no nome do seu novo jogo, MadLands: O Deserto em Fúria. Ele virá por financiamento coletivo.
 * A Conclave Editora iniciou no dia 6 de Janeiro uma “mega pré-venda” de aniversário com cinco títulos: Barony, Barony Sorcery, Room-25, Room-25 Season 2 e Ultimate Warriorz.
 * Ludofy informa que o jogo Patchwork esgotou-se e estão providenciando nova impressão.

RPG
---------------------
 * O financiamento coletivo da nova impressão de Rastro de Cthulhu está terminando e até o momento da edição não havia alcançado a meta básica.
 * A edição 549 do Nerdcast foi o início de uma aventura chamada O Mistério de William Faraday usando o sistema Call of Cthulhu mestrado por Leonel Caldela.

Editores: Cláudio Torcato e Gabriel Bragança
	""",
	16: """
Boletim Semanal de Notícias N° 16 (26.12.16 - 01.01.17)

Board Game
---------------------
 * Twilight Imperium foi o vencedor do Prêmio Cajuína Meeple Edição 2º Semestre de 2016. Figuraram no Top 5 também: Eldritch Horror, A Game of Thrones: The Board Game, Merchants & Marauders e Twilight Struggle
 * Funbox Jogos mudou de nome. Agora se chama Mandala Jogos
 
RPG
---------------------
 * Retropunk Publicações divulga que o Savage Worlds Adventure Pack de novembro já está disponível com quatro aventuras rápidas para Accursed.
 * Redbox Editora divulga capa da aventura exclusiva que virá na caixa básica de Space Dragon
 * Saiu a primeira edição da revista digital Iniciativa 3D&T Alpha, um trabalho conjunto de sites interessados em divulgar material para o sistema
 * O Lampião Game Studio divulga um novo RPG a ser publicado pela AVEC Editora: Pesadelos Terríveis

Editores: Cláudio Torcato e Gabriel Bragança	
	""",
	15: """
Boletim Semanal de Notícias N° 15 (19.12.16 - 25.12.16)

Board Game
---------------------
 * Campanha de financiamento coletivo do Conexão Hacker Card Game teve início pelo Kickante.
 * Conclave Editora anuncia para o início de 2017 o jogo Room 25 e sua expansão Room 25: Season 2 e mais outros jogos.
 * Conclave Editora informa que os jogos Caçadores da Galáxia e Sonhando com Alice fazem agora parte de seu acervo.
 * Ludofy apresenta novo produto para 2017, Viticulture, que em março, estará no mercado nacional. Você pode ler uma resenha do jogo em nosso blog.
 * A Meeple BR Jogos anuncia para início de 2017 o jogo Labyrinth: The Board Game, baseado no filme homônimo que completará 30 anos de sua estreia.

Editores: Cláudio Torcato e Gabriel Bragança
	""",
	14: """
Boletim Semanal de Notícias N° 14 (12.12.16 - 18.12.16)

Board Game
---------------------
 * O jogo Cartas a Vapor foi lançado oficialmente no dia 18 de Dezembro na Funbox Ludolocadora
 * Meeple BR Jogos divulga a pré-venda do Fireteam Zero no Facebook
 * Conclave Editora divulga um novo jogo: Ultimate Warriorz
 * Funbox Jogos informa que as expansões do Dwar7s estão confirmadas para 2017

RPG
----------------------
 * Diogo Nogueira, autor do blog Pontos de Experiência, lançou no site RPG Now, o jogo Sharp Swords & Sinisters Spells, pela modalidade “pague o quanto quiser”.

Editores: Cláudio Torcato e Gabriel Bragança
	""",
	13: """
Boletim Semanal de Notícias N° 13 (05.12.16 - 11.12.16)


Board Game
---------------------
 * Ludofy anuncia em post no Facebook que em janeiro teremos Celestia no mercado nacional
 * Sator Arepo Tene Opera Rodas começa a ser enviado aos compradores, segundo post informativo da Ludofy


RPG
----------------------
 * Redbox publicou um post atualizando o status de diversos de seus jogos em pré-venda
 * Editora Daemon voltará a imprimir seus jogos clássicos: Arkanun, Trevas, Invasão, entre outros.
 * A campanha de financiamento coletivo do 7º Mar já alcançou sua meta básica e mais duas estendidas.


Editores: Cláudio Torcato e Gabriel Bragança
	""",
	12: """
Boletim Semanal de Notícias N° 12 (28.11.16 - 04.12.16)


Board Game
---------------------
 * No último sábado, dia 3, aconteceu a 1ª edição do Cajuína Meeple, evento que visa divulgar jogos de tabuleiro modernos e reunir os fãs do hobby, na Livraria Leitura do Shopping Rio Poty.
 * Pensamento Coletivo inicia pré-venda de Imperial 2030. Envios previstos para Dezembro/16
 * Conclave Editora inicia pré-venda de Jórvik. Envios previstos para Janeiro/17
 * Histeria Games inicia envios do financiamento de Sonhando com Alice
 * Bucaneiro Jogos revela lançamento de sua linha de sleeves


RPG
----------------------
 * Devir anuncia lançamento do Bestiário de Pathfinder. Livro já estará disponível na Comic Con Experience
 * Jambô inicia pré-venda do mangá Khalifor Vol. 1, inspirado em Tormenta
 * Jambô inicia pré-venda da antologia de contos, Contos de Tormenta Vol. 2.


Editores: Cláudio Torcato e Gabriel Bragança
	""",
	11 : """
Boletim Semanal de Notícias Nº 11 (21.11.16 - 27.11.16)


Board Game
----------------------
 * FunBox inicia pré-venda de Imperial Settlers e algumas mini expansões. Previsão de envio para 05/12


RPG
----------------------
 * Saiu a Conexão Fate Edição Adaptação Nº 2, revista eletrônica especializada no sistema Fate
 * Encontra-se em financiamento coletivo o livro Histórias do Brasil - Sistema de RPG para Uso Escolar de Neemias Guimarães


Editores: Cláudio Torcato e Gabriel Bragança	
	""",
	10 : """
Boletim Semanal de Notícias Nº 10 (14.11.16 - 20.11.16)


Board Game
----------------------
 * FunBox inicia pré-venda de Lisboa (com extras do financiamento coletivo americano)
 * Conclave Editora e Histeria Games anunciam parceria 
 * Ace Studios anuncia data para início de envios do Space Cantina. Dia 24/11
 * Editora Meeple BR libera Mombasa para pré-venda

RPG
----------------------
* A campanha de financiamento coletivo da aclamada 2ª edição de 7th Sea começou no Brasil via Catarse. Quem traz o jogo é o pessoal da New Order Editora que o publicará como Sétimo Mar.
* Outro financiamento que começou na semana foi o Guia de Classes, um suplemento para  Old Dragon, saindo pela Moostache Books via Catarse.
* Shadows of the Demon Lord tornou-se a maior campanha de financiamento coletivo de um jogo de RPG no Brasil.
* A campanha de financiamento do The Esoterrorists deve ficar para o primeiro trimestre de 2017 


Editores: Cláudio Torcato e Gabriel Bragança
	""",
	9: """
Boletim Semanal de Notícias Nº 9 (07.11.16 - 13.11.16)


Board Game
----------------------
 * Ace Studio divulga seu próximo jogo: Medievália.
 * A campanha de financiamento coletivo de Swords and Bagpipes teve início
 * Redbox inicia pré venda de Boss Monster

RPG
----------------------
 * Jambô Editora divulga as datas dos lançamentos de diversos produtos, dentre eles Dragon Age.
 * A New Order Editora divulgou no Facebook que o financiamento coletivo do 7th Sea começa no final de Novembro.
 * O financiamento coletivo da nova impressão de Rastro de Cthulhu e novos suplementos começou no dia 11. [colocar o link]


Editores: Cláudio Torcato e Gabriel Bragança
	""",
	8: """
Boletim Semanal de Notícias Nº 8 (31.10.16 - 06.11.16)


Board Game
----------------------
 * Na última semana ocorreram os campeonatos mundiais da Fantasy Flight Games. Os jogos do mundial foram: Warhammer 40k: Conquest, SW: LCG, Android: Netrunner LCG, A Game of Thrones LCG, Imperial Assault, SW X-Wing, SW Armada e partidas exibição do SW Destiny.
 * FunBox libera pré venda de Terra Mystica: Fogo e Gelo (Expansão), Terrenos Especiais (mini expansão de Terra Mystica)  e Good Cop Bad Cop. Previsão de envios para o dia 10/11
 * Em nota, Galápagos oficializa (e explica) atraso de Mansions of Madness 2ª edição. Agora, jogo tem lançamento com data para primeiro trimestre de 2017
 * RedBox surpreende e divulga parceria com a editoria Mais que Oca (Chariots Race, Concórdia, Keyflower, Camel Up, K2).
 * FunBox anuncia valores de Imperial Settlers, Pré venda - R$199,90 / Valor normal - R$219,90. Pré venda está próxima.
 * RedBox anuncia Labyrinx. Jogo terá pré venda no Catarse ainda em 2016. 
 * MS Jogo dá início a pré venda de Aquarium dia 07/11. Promoção terá frete grátis até dia 11/11.


RPG
----------------------
 * New Order Editora solta uma nota críptica na sua página do Facebook sobre algo que pode ser anunciado/iniciado ainda este mês. Ao que parece é o 7th Sea.
 * Shadow of the Demon Lord bateu a meta de 40 mil Reais no Catarse.me.
 * O jogo brasileiro, Déloyal, foi financiado com sucesso no Catarse.me. 
 * A Redbox Editora anuncia que a versão do Old Dragon será gratuita em homenagem aos 6 anos da editora.


Editores: Cláudio Torcato e Gabriel Bragança
	""",
	7: """
Boletim Semanal de Notícias Nº 7 (24.10.16 - 30.10.16)

Board Game
----------------------
 * Pensamento Coletivo anuncia lançamento de Rise to Power ainda sem data definida
 * Galápagos Jogos confirma lançamento de Star Wars Destiny para dezembro. Teremos demonstrações e amostras do jogo na Comic Con Experience (Lançamento também previsto para ser na Comic Con, porém não é confirmado) 
 * Fire on Board confirma que vai trazer Scythr e já inicia pré venda no dia 02/11. Tiragem terá apenas 80 unidades

RPG
----------------------
 * Lampião Game Studio divulga seu próximo produto, Travessias, um RPG criado coletivamente que prevê rodízio de mestres entre as sessões e criação conjunta do enredo da crônica. Será disponibilizado gratuitamente. 
 * Jambô Editora apresenta a capa da primeira edição da volta da Dragão Brasil

Editores: Cláudio Torcato e Gabriel Bragança
	""",
	6: """
Boletim Semanal de Notícias Nº 6 (17.10.16 - 23.10.16)


Board Game
----------------------
 * Anunciado Dirigíveis Vigaristas (Dastardly Dirigibles - nome original), primeiro jogo da nova editora É Jogo
 * Editora RetroPunk anuncia o card game/party game Fat Food
 * Conclave Editora anunciou Jórvik para esse ano ainda, repõe estoque do jogo Mexica e inicia pré venda de 4 Gods
 * Galápagos surpreende e lança Mysterium e Loony Quest sem pre venda para dia 23/10
 * Entre os dias 2 e 6 de Novembro, a Fantasy Flight Games organizará os Campeonatos Mundiais de seus jogos competitivos, tais como: A Game of Thrones: The Card Game, Android: Netrunner, Star Wars: Armada, Star Wars: Imperial Assault, Star Wars: The Card Game, Warhammer 40k: Conquest e X-Wing.


RPG
----------------------
 * Pensamento Coletivo mostrou uma imagem de algum novo produto para Jadepunk no Facebook
 * Pré-venda do ICONS começa dia 26 pela Redbox Editora
 * A Redbox divulga um poster sobre o evento Old Dragon Day com data para o dia 13 de Novembro. O que será Thordezilhas: Sabres & Caravelas? Um cenário ou um novo jogo?


Editores: Cláudio Torcato e Gabriel Bragança
	""",
	5: """
Boletim Semanal de Notícias Nº 5 (10.10.16 - 16.10.16)


Board Game
----------------------
 * Anunciada pré-venda de SeaFall para dia 17/10
 * Boato se confirma e a Kronos Games vai trazer Tyrants of the Underdark (universo Dungeons and Dragons) em português 
 * Galápagos anuncia que pré-venda de Mysterium está próxima 
 * Conclave Editora aumenta rumor sobre lançamentos de Tikal e Amun-re

RPG
----------------------
 * Dragão Brasil voltará em formato digital no dia 9 de Novembro. Tenha acesso às edições via Apoia.se.
 * Allison Vitório, um dos participantes mais influentes da comunidade Dungeon World Brasil será editor da linha de produtos do Dungeon World na editora Secular Games.
 * Foices & Feitiços foi financiado com sucesso e o prazo já está terminando.


Editores: Cláudio Torcato e Gabriel Bragança
	""",
	4: """
Boletim Semanal de Notícias Nº 4 (03.10.16 - 09.10.16)


Board Game
---------------------
 * Inscrições abertas para a 3ª etapa da THE X-Wing, a liga teresinense de X-Wing
 * Battle Royal 2016 é anunciado pela Copag. É um torneio do card game Marvel Battle Scenes que acontecerá em São Paulo.
 * Confirmado lançamento no Brasil de Heroes of Normandie pela New Order Editora. Lançamento ainda indefinido
 * Conclave anuncia 4 Gods.
 * MS Jogos anuncia para o dia 9 de Novembro a pronta-entrega de Aquarium, o primeiro relançamento do jogo
 * Galápagos anuncia lançamento de Potion Explosion e reposição de estoque de Blood Rage para dia 10 de outubro 

RPG
---------------------
 * Cyber Mamute, um zine sobre RPG indie, está de volta

Editores: Cláudio Torcato e Gabriel Bragança
	""",
	3: """
Boletim Semanal de Notícias Nº 3 (26.09.16 - 02.10.16)


Board Game
----------------------
 * Segunda edição de Aquarium será lançada no dia 7 de Novembro com pronta-entrega
 * Sator entra em pré-venda pela Ludofy
 * Galápagos divulga datas e locais para o novo regional de X-Wing
 * Revelada a Segunda edição de Dominion e com confirmação do lançamento em Português pela Conclave Editora 
 * Redbox Editora iniciou no dia 28/09 a pré-venda do Boss Monster
 * O designer Fábio Melo iniciou a pré-venda do Gasolina de Sangue
 * Anunciado TCG baseado na franquia de Final Fantasy 
 * Mechs and Minions, baseado no League of Legends, será lançado lá fora pela Riot Games

RPG
----------------------
 * A segunda edição de Apocalypse World foi lançada na DrivethruRPG (essa edição era esperada para finalmente o jogo chegar em português)
 * Redbox Editora divulga as pré-vendas do suplemento Guia de Raças para Old Dragon e o RPG Icons que começarão em Outubro
 * Pensamento Coletivo lança a pré-venda de Déloyal via Catarse, um RPG de resistência e liberdade, desenvolvido pelo Lampião Game Studio.
 * Retropunk inicia a pré-venda de Terra Devastada: Edição Apocalipse

Editores: Cláudio Torcato e Gabriel Bragança
	""",
	2: """
Boletim Semanal de Notícias Nº 2 (19.09.16 - 25.09.16)


Board Games
----------------------
* Hugo Quintas venceu o Torneio de X-Wing da Arcádia que ocorreu no dia 25 de setembro. Participaram 12 pilotos e pilotas.
* Neuroshima Hex, Quartz, Coup e Dead of Winter voltam aos estoques das lojas
* Lançamentos de Black Stories 3, Black Stories Funny Death, Black Stories Shit Happens, Munchkin Panic
* Retropunk planeja lançar um board game inspirado no RPG Terra Devastada
* Lançamentos futuros da Pensamento Coletivo divulgados na WRF 2016: La Granja e Don’t Turn Your Back
* Lançamento futuro da Jambô Editora divulgado na WRF 2016: nova edição de Dragon Age (final do ano)
* A Red Box iniciou a pré-venda do jogo Ca$h’n Guns.

RPGs
----------------------
* Mighty Blade 3ª Edição em financiamento coletivo no Catarse.
* Dungeon Fantasy da Steve Jackson Games alcançou a meta do financiamento coletivo via Kickstarter
* Pré-venda de O Senhor das Sombras, segundo livro jogo baseado em Tormenta RPG pela Jambô Editora
* Retropunk planeja financiamento do Esoterroristas para o final do ano
* Retropunk planeja financiamento da segunda impressão de Rastro de Cthulhu via financiamento coletivo (metas estendidas: Escudo do Guardião, a campanha Eternal Lies, Shadows over Filmland, Arkham Detective Tales: Extended Edition)
* Retropunk planeja financiamento coletivo para cenários de Savage Worlds em dezembro de 2016
* Retropunk divulga que estão assumindo a linha de Castle Falkenstein e poderão ter algo para 2017
* Retropunk divulga outro jogo da Pelgrane Press para 2017: Night’s Black Agents

Editores: Cláudio Torcato e Gabriel Bragança
	""",
	1: """
Boletim BGamers Nº 1 (11.09.16 - 18-09-16)


Board Games
-----------------------
Divulgado na World RPG Fest, La Granja sairá pela Pensamento Coletivo
Galápagos anuncia Munchkin Panic
Galápagos Jogos divulga que expansões para Black Stories serão lançadas na semana seguinte 
Red Box com a pré-venda do Rock’m Roll Manager


RPGs
-----------------------
Terra Devastada Edição Apocalipse  foi lançada no World RPG Fest
Dia 16 terminou a pré-venda da caixa introdutória de Shadowrun
Dia 15 teve início a pré-venda dos Senhores da Guerra, suplemento para Old Dragon pela Red Box Editora
Dia 16 começou a pré-venda de Shadow of the Demon Lord pela Pensamento Coletivo
Solar Entretenimento divulga o status de vários suplementos para FATE que saíram graças ao financiamento coletivo
Edição #2 da Conexão Fate
World RPG Fest em Curitiba, dias 17 e 18 de setembro
	"""
}