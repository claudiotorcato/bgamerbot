# -*- coding: utf-8 -*-
indicados = {
	"2.2017" : """
Vencedores do Prêmio Cajuína Meeple
Edição: 1º Semestre de 2017
---------------------------------------
1 - Empires: Age of Discovery
2 - Cyclades
3 - Lords of Waterdeep
4 - Guerra do Anel
5 - Through the Ages: A New Story of Civilization

Indicados para o Prêmio Cajuína Meeple 
Edição: 2º Semestre de 2017
---------------------------------------
 1 - Carson City
 2 - Trajan
 3 - Scippio
 4 - Russian Railroads
 5 - Raiders of the North Sea
 6 - The Manhatan Project
 7 - Village
 8 - Kemet
 9 - Terraforming Mars
 10 - Time Stories
 11 - Orléans
 12 - The Castles of Burgundy
 13 - Mansion of Madness
 14 - Fireteam Zero
 15 - Agricola
	""",
	"2.2016" : """
Vencedores do Prêmio Cajuína Meeple
Edição: 2º Semestre de 2016
---------------------------------------
1 - Twilight Imperium
2 - Eldritch Horror
3 - Game of Thrones Board Game
4 - Merchants & Marauders
5 - Twilight Struggle

Indicados para o Prêmio Cajuína Meeple 
Edição: 2º Semestre de 2016
---------------------------------------
 1 - Game of Thrones Board Game
 2 - Jester
 3 - Galaxy Trucker
 4 - Twilight Struggle
 5 - Rock'n Roll Manager
 6 - Power Grid
 7 - Twilight Imperium
 8 - Merchants & Marauders
 9 - Eldritch Horror
 10 - Zombicide Black Plague
 11 - Isle of Skye
 12 - Drillit
 13 - The Gallerist
 14 - Alquimistas
 15 - T'Zolkin
	""",
}