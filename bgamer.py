# -*- coding: utf-8 -*-
import sys
import asyncio
from functools import reduce
import telepot
from telepot import glance, message_identifier
from telepot.aio.helper import InlineUserHandler, AnswererMixin
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton
from telepot.aio.delegate import (
	per_chat_id ,per_application, per_inline_from_id, call, create_open, 
	pave_event_space, include_callback_query_chat_id, per_callback_query_origin)
from boletins import BOLETINS
from premio import indicados
from pony.orm import *
from models import Eleicao, Usuario, Jogo, Voto, Sugestao

class PremioStore(object):

	@db_session
	def __init__(self):
		try:
			self.eleicao = Eleicao[1]
		except ObjectNotFound as e:
			self.criar_eleicao()

	@db_session
	def criar_eleicao(self):
		self.eleicao = Eleicao(nome="Prêmio Cajuína Meeple", edicao="2.2016")
		Jogo(nome="Game of Thrones Board Game",eleicao=self.eleicao)
		Jogo(nome="Jester",eleicao=self.eleicao)
		Jogo(nome="Galaxy Trucker",eleicao=self.eleicao)
		Jogo(nome="Twilight Struggle",eleicao=self.eleicao)
		Jogo(nome="Rock'n Roll Manager",eleicao=self.eleicao)
		Jogo(nome="Power Grid",eleicao=self.eleicao)
		Jogo(nome="Twilight Imperium",eleicao=self.eleicao)
		Jogo(nome="Merchants & MaraudersEldritch Horror",eleicao=self.eleicao)
		Jogo(nome="Eldritch Horror",eleicao=self.eleicao)
		Jogo(nome="Zombicide Black Plague",eleicao=self.eleicao)
		Jogo(nome="Isle of Skye",eleicao=self.eleicao)
		Jogo(nome="Drillit",eleicao=self.eleicao)
		Jogo(nome="The Gallerist",eleicao=self.eleicao)
		Jogo(nome="Alquimistas",eleicao=self.eleicao)
		Jogo(nome="T'Zolkin",eleicao=self.eleicao)
		commit()

	@db_session
	def candidatos(self):
		lista = Jogo.select(lambda j: j.eleicao == self.eleicao)[:] 
		return [ j.id for j in lista]

	@db_session
	def meus_votos(self, user_id):
		lista = Voto.select(lambda v: v.eleitor.user_id == user_id)[:] 
		return [ v.jogo.id for v in lista ]

	@db_session
	def meus_votos_nomes(self, user_id):
		lista = Voto.select(lambda v: v.eleitor.user_id == user_id)[:] 
		return lista

	@db_session
	def meu_ranking(self, user_id):
		e = Eleicao[self.eleicao.id]
		lista = Voto.select(lambda v: v.eleitor.user_id == user_id
			and v.eleicao == e).order_by(Voto.posicao)[:] 
		texto = ''
		for v in lista:
			texto += "%s - %s\n" % (v.posicao, v.jogo.nome)
		return texto

	@db_session
	def limpar_votos(self, user_id):
		e = Eleicao[self.eleicao.id]
		delete(v for v in Voto if v.eleicao == e and v.eleitor.user_id == user_id)
		commit()

	@db_session
	def escolher(self, user_id, jogo_id):
		e = Eleicao[self.eleicao.id]
		jogo = Jogo[jogo_id]
		usuario = Usuario.get(user_id=user_id)
		if not usuario:
			usuario = Usuario(user_id=user_id)
		voto = Voto.get(eleicao=e,eleitor=usuario,jogo=jogo)
		if not voto:
			ranking = max(v.posicao for v in Voto if v.eleicao == self.eleicao 
				and v.eleitor == usuario)
			if not ranking:
				ranking = 0
			voto = Voto(eleicao=e,eleitor=usuario,jogo=jogo,posicao=ranking+1)
		commit()
		return voto

	@db_session
	def busca_jogo(self, id):
		return Jogo[id]

	@db_session
	def atualizar_primeiro_nome(self, user_id, nome):
		usuario = Usuario.get(user_id=user_id)
		if not usuario:
			usuario = Usuario(user_id=user_id)
		usuario.nome = nome

	@db_session
	def pode_votar(self, user_id):
		usuario = Usuario.get(user_id=user_id)
		if not usuario:
			usuario = Usuario(user_id=user_id)
		usuario.pode_votar = True

	@db_session
	def numero_votos(self):
		lista = select(v.eleitor for v in Voto)
		return len(lista)


class SugestaoStore(object):

	@db_session
	def registrar_sugestao(self, user_id, group, descricao, nome):
		usuario = Usuario.get(user_id=user_id)
		if not usuario:
			usuario = Usuario(user_id=user_id, nome=nome)
		else:
			usuario.nome = nome
		commit()
		sugestao = Sugestao.get(usuario=usuario,grupo=group)
		if sugestao:
			sugestao.descricao = descricao
		else:
			Sugestao(descricao=descricao,usuario=usuario,grupo=group)

	@db_session
	def listar_sugestoes(self, grupo):
		lista = select((s.usuario.nome, s.descricao) for s in Sugestao if s.grupo == grupo)[:]
		texto = ''
		for s in lista:
			texto += "%s - %s\n" % (s[0], s[1])
		return texto


class BoletimStore(object):
	def __init__(self):
		self._db = BOLETINS

	def get(self, number):
		if number in self._db.keys():
			return self._db[number]
		else:
			return None

	def last(self):
		return self._db[ max(self._db.keys()) ]

class SugestaoHandler(telepot.aio.helper.ChatHandler):
	def __init__(self, *args, **kwargs):
		super(SugestaoHandler, self).__init__(*args, **kwargs)
		self.esperando_sugestao = False
		self._store = SugestaoStore()

	async def on_chat_message(self, msg):
		content_type, chat_type, chat_id, momento, message_id = telepot.glance(msg, long=True)
		if content_type != 'text':
			return

		command = msg['text'].strip().lower().split()
		if '/sugestao' in command or '/sugestao@bgamer_bot' in command:
			if chat_id != -153435790:
				await self.sender.sendMessage("""
					Comando exclusivo do grupo RPG Grande Teresina.
					Junte-se a nós https://telegram.me/joinchat/Bi4_6wklPo6cS0nx3oiQXw""")
				return
			self.esperando_sugestao = True
			await self.sender.sendMessage('Escreve sua sugestão')
			return
		if self.esperando_sugestao and chat_type == 'group':
			self._store.registrar_sugestao(msg['from']['id'], chat_id, msg['text'],
				msg['from']['first_name'])
			await self.sender.sendMessage('Obrigado!')
			self.esperando_sugestao = False

	async def on__idle(self, event):
		if self.esperando_sugestao:
			await self.sender.sendMessage('Sugestão não atualizada')
		self.close()

	def on_close(self, ex):
		self.close()

class OwnerHandler(telepot.aio.helper.ChatHandler):
	def __init__(self, seed_duple, **kwargs):
		super(OwnerHandler, self).__init__(seed_duple, **kwargs)
		self._store = BoletimStore()
		self._store1 = SugestaoStore()

	async def on_chat_message(self, msg):
		content_type, chat_type, chat_id, momento, message_id = telepot.glance(msg, long=True)

		if content_type == 'new_chat_member' and chat_type == 'group':
			nome = msg['from']['first_name']
			await self.sender.sendMessage('Olá, %s' % (nome,))
			return
		if content_type != 'text':
			return

		print('Chat Id', chat_id, 'Chat Type', chat_type)

		command = msg['text'].strip().lower().split()

		if '/boletim' in command or '/boletim@bgamer_bot' in command:
			if len(command) < 2:
				result = self._store.last()
				await self.sender.sendMessage(result)
			else:
				try:
					number = int(command[1])
				except ValueError:
					await self.sender.sendMessage('Não entendi')
					return
				result = self._store.get(number)
				if not result:
					await self.sender.sendMessage('Boletim não encontrado')
					return
				await self.sender.sendMessage(result)

		if '/indicados' in command or '/indicados@bgamer_bot' in command:
			await self.sender.sendMessage(indicados['2.2017'])

		if '/papo_da_semana' in command or '/papo_da_semana@bgamer_bot' in command:
			if chat_id != -153435790:
				await self.sender.sendMessage("""
					Comando exclusivo do grupo RPG Grande Teresina.
					Junte-se a nós https://telegram.me/joinchat/Bi4_6wklPo6cS0nx3oiQXw""")
				return
			else:
				await self.sender.sendMessage('O papo da semana não tem tópico central.')

		if '/sugestoes' in command or '/sugestoes@bgamer_bot' in command:
			if chat_id != -153435790:
				await self.sender.sendMessage("""
					Comando exclusivo do grupo RPG Grande Teresina.
					Junte-se a nós https://telegram.me/joinchat/Bi4_6wklPo6cS0nx3oiQXw""")
				return
			texto = self._store1.listar_sugestoes(chat_id)
			await self.sender.sendMessage('Sugestões\n--------\n%s' % (texto,))

	def on__idle(self, event):
		self.close()

	def on_close(self, ex):
		self.close()

class VotacaoStarter(telepot.aio.helper.ChatHandler):
	def __init__(self, *args, **kwargs):
		super(VotacaoStarter, self).__init__(*args, **kwargs)
		self._store = PremioStore()

	async def on_chat_message(self, msg):
		content_type, chat_type, chat_id = glance(msg)
		if content_type != 'text':
			return
		command = msg['text'].strip().lower().split()
		if '/votar' in command or '/votar@bgamer_bot' in command:
			#m = telepot.namedtuple.Message(**msg)
			#chatMember = self.bot.getChatMember(chat_id, m.from_)
			#print(chatMember)
			self._store.atualizar_primeiro_nome(msg['from']['id'], msg['from']['first_name'])
			if chat_type == 'private':
				await self.sender.sendMessage(
					'Pressione *INICIO* para entrar no módulo de votação',
					parse_mode='Markdown',
					reply_markup=InlineKeyboardMarkup(
						inline_keyboard=[[
							InlineKeyboardButton(text='INICIO', callback_data='start'),
						]]
					)
				)
			else:
				await self.sender.sendMessage('Converse no modo privado comigo para votar')

		self.close()  # let Quizzer take over
	def on_close(self, ex):
		self.close()


class Votacao(telepot.aio.helper.CallbackQueryOriginHandler):
	def __init__(self, *args, **kwargs):
		super(Votacao, self).__init__(*args, **kwargs)
		self._store = PremioStore()

	async def _show_next_question(self, from_id):
		votos = self._store.meus_votos(from_id)
		buttons = self.montar_opcoes(from_id)
		keyboard = InlineKeyboardMarkup(inline_keyboard= buttons)
		if len(votos) >= 5:
			question = ''
		else:
			question = """
Prêmio Cajuína Meeple
Edição 2/2016
Nº de eleitores: %d
Selecione o jogo nº %d do seu Top 5
			""" % (self._store.numero_votos(), len(votos) + 1)
		await self.editor.editMessageText(question,
			reply_markup=InlineKeyboardMarkup(
				inline_keyboard=buttons)
		)		

	def montar_opcoes(self, from_id):
		votos = self._store.meus_votos(from_id)
		candidatos = self._store.candidatos()
		buttons = [ [InlineKeyboardButton(text=self._store.busca_jogo(x).nome, callback_data=str(x))] for x in list(set(candidatos) - set(votos)) ]
		buttons.append(
			[InlineKeyboardButton(text='Meus Votos', callback_data='listar'),
			 InlineKeyboardButton(text='Sair', callback_data='sair')]
		)
		return buttons


	async def on_callback_query(self, msg):
		query_id, from_id, query_data = glance(msg, flavor='callback_query')
		if query_data not in ['start','listar','voltar','limpar','sair']:
			voto = self._store.escolher(int(from_id), int(query_data))
			if len(self._store.meus_votos(int(from_id))) < 5:
				await self._show_next_question(int(from_id))
			else:
				await self._sem_indicacao()
		elif query_data == 'listar':
			await self._meus_votos(int(from_id))
		elif query_data == 'voltar':
			if len(self._store.meus_votos(int(from_id))) < 5:
				await self._show_next_question(int(from_id))
			else:
				await self._sem_indicacao()
		elif query_data == 'limpar':
			self._store.limpar_votos(int(from_id))
			await self._show_next_question(int(from_id))
		elif query_data == 'sair':
			await self.editor.editMessageText('Obrigado por votar', reply_markup=None)
			self.close()
		else: # start
			if len(self._store.meus_votos(int(from_id))) < 5:
				await self._show_next_question(int(from_id))
			else:
				await self._sem_indicacao()

	async def _sem_indicacao(self):
		buttons = [
			[InlineKeyboardButton(text='Meus Votos', callback_data='listar'),
			 InlineKeyboardButton(text='Sair', callback_data='sair')]
		]
		keyboard = InlineKeyboardMarkup(inline_keyboard= buttons)
		question = "Prêmio Cajuína Meeple\nEdição 2/2016\nNº de eleitores: %d" % (self._store.numero_votos(),)
		await self.editor.editMessageText(question,
			reply_markup=InlineKeyboardMarkup(
				inline_keyboard=buttons)
		)		

	async def _meus_votos(self, from_id):
		from pprint import pprint
		texto = self._store.meu_ranking(from_id)
		buttons = [
			[InlineKeyboardButton(text='Voltar', callback_data='voltar'),
			 InlineKeyboardButton(text='Limpar', callback_data='limpar')]
		]
		keyboard = InlineKeyboardMarkup(inline_keyboard= buttons)
		question = "*Meu Top 5* \n\n%s" % (texto,)
		await self.editor.editMessageText(question,
			parse_mode='Markdown',
			reply_markup=InlineKeyboardMarkup(
				inline_keyboard=buttons)
		)		

	async def on__idle(self, event):
		text = 'Até a próxima'
		await self.editor.editMessageText(text, reply_markup=None)
		votos = []
		self.close()

	def on_close(self, ex):
		self.close()


TOKEN = sys.argv[1]

bot = telepot.aio.DelegatorBot(TOKEN, [
    pave_event_space()(
        per_chat_id(), create_open, OwnerHandler, timeout=10),
    pave_event_space()(
        per_chat_id(), create_open, SugestaoHandler, timeout=10),
#    pave_event_space()(
#        per_chat_id(), create_open, VotacaoStarter, timeout=3),
#    pave_event_space()(
#        per_callback_query_origin(), create_open, Votacao, timeout=30),
])

loop = asyncio.get_event_loop()
loop.create_task(bot.message_loop())
print('Listening ...')

loop.run_forever()